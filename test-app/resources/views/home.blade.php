@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="card-columns">
                        <div class="card text-white bg-primary mb-3" style="max-width: 18rem;">
                        <div class="card-header">{{__('Current balance')}}</div>
                            <div class="card-body">
                            <h5 class="card-title">{{ $totalBalance }}</h5>
                            </div>
                        </div>
                        <div class="card text-white bg-primary mb-3" style="max-width: 18rem;">
                            <div class="card-header">{{__('Nr. of Refferals')}}</div>
                                <div class="card-body">
                                <h5 class="card-title">{{ $totalRefferals }}</h5>
                            </div>
                        </div>
                        <div class="card text-white bg-primary mb-3" style="max-width: 18rem;">
                            <div class="card-header">{{__('Earned from Refferals')}}</div>
                                <div class="card-body">
                                <h5 class="card-title">{{ $totalEarnedFromRefferals }}</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
