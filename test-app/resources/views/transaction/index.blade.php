@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('All my transactions') }}</div>

                <div class="card-body">
                    @if(count($allTransactions) > 0 )
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Ammount</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Type</th>
                                    <th scope="col">Date</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($allTransactions as $transaction)
                                <tr>
                                    <th scope="row">{{ $transaction->id }}</th>
                                    <td>{{ $transaction->ammount }}</td>
                                    <td>{{ $transaction->status }}</td>
                                    <td>{{ $transaction->type }}</td>
                                    <td>{{ $transaction->created_at }}</td>
                                </tr>
                            @endforeach
                                <tr>
                                    <th scope="row">{{ __('Total') }}</th>
                                    <td>{{ $totalBalance }}</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    @else 
                        {{__('No transactions at the moment!') }}
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
