@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Refill your account balance') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('transaction.store') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="ammount" class="col-md-4 col-form-label text-md-right">{{ __('Ammount') }}</label>

                            <div class="col-md-6">
                                <input id="ammount" type="number" class="form-control" name="ammount" value="{{ old('ammount') }}" min="10" required autofocus>

                                @if ($errors->has('ammount'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('ammount') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Refill') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
