<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRefferalLinkIdUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('refferal_token', 15)->after('remember_token');
            $table->integer('refferal_id')->after('refferal_token')->nullable();
            $table->float('total_balance', 10, 2)->after('refferal_id')->default('0.00');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('refferal_token');
            $table->dropColumn('refferal_id');
            $table->dropColumn('total_balance');
        });
    }
}
