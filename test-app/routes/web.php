<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home');
Route::get('register/{refferal}', 'Auth\RegisterController@showRefferalRegistrationForm')->name('register-refferal-form');

Route::middleware(['auth'])->group(function () {
    Route::get('/user/{user}/profile', 'UserProfileController@view')->name('profile');
    Route::post('/user/{user}/update-profile', 'UserProfileController@updateProfile')->name('update-profile');
    Route::post('/user/{user}/update-password', 'UserProfileController@updatePassword')->name('update-password');

    Route::resource('transaction', 'TransactionController')->except([
        'destroy'
    ]);
});