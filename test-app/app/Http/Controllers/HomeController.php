<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $totalBalance = \Auth::user()->getBallance();
        $totalRefferals = \Auth::user()->totalRefferals();
        $totalEarnedFromRefferals = \Auth::user()->totalEarnedFromRefferals();

        return view('home', compact(['totalBalance', 'totalRefferals', 'totalEarnedFromRefferals']));
    }
}
