<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Hash;

class UserProfileController extends Controller
{
    /**
     * Function that render the user profile
     */
    public function view(\App\User $user) {

        return view('user.profile', compact('user'));
    }

    /**
     * Function that update user profile
     */
    public function updateProfile(Request $request, \App\User $user) {
        $validatedData = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255',  Rule::unique('users')->ignore($user->id)],
        ]);

        $user->update($validatedData);

        $request->session()->flash('message', 'Profile updated with success!');

        return redirect()->route('profile', ['id' => $user->id]);
    }

    /**
     * Function that update user password
     */
    public function updatePassword(Request $request, \App\User $user) {
        $validatedData = $request->validate([
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);

        $user->update(['password' => Hash::make($validatedData['password'])]);

        $request->session()->flash('message', 'Password updated with success!');

        return redirect()->route('profile', ['id' => $user->id]);
    }
}
