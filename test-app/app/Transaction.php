<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    const TRANS_STATUS_SUCCESS = 'success';
    const TRANS_STATUS_FAIL = 'fail';
    const TRANS_STATUS_PROCESSING = 'processing';
    const TRANS_STATUS_REFUND = 'refund';

    const TRANS_TYPE_SELF = 'self';
    const TRANS_TYPE_REFFERAL = 'refferal';

    const REFFERAL_PERCENTAGE = 10;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'ammount', 'status', 'type'
    ];

    public function user()
    {
        return $this->belongsTo(\App\User::class, 'user_id', 'id');
    }

    public static function registerTransaction(float $ammount, string $status)
    {
        self::create([
            'ammount' => $ammount,
            'status' => $status,
            'type' => self::TRANS_TYPE_SELF,
            'user_id' => \Auth::id()
        ]);

        if ($status === self::TRANS_STATUS_SUCCESS && !empty(\Auth::user()->referrer->id) ) {
            self::create([
                'ammount' => $ammount*self::REFFERAL_PERCENTAGE/100,
                'status' => $status,
                'type' => self::TRANS_TYPE_REFFERAL,
                'user_id' => \Auth::user()->referrer->id
            ]);
        }
    }
}
