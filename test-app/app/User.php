<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'refferal_id', 'refferal_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function referrer() {
        return $this->belongsTo(\App\User::class, 'refferal_id', 'id');
    }
    
    public function referrals() {
        return $this->hasMany(self::class, 'refferal_id', 'id');
    }

    public function getBallance() {
        return DB::table('transactions')->where('user_id', \Auth::id())->sum('ammount') ?? 0;
    }

    public function totalRefferals() {
        return DB::table('users')->where('refferal_id', \Auth::id())->count() ?? 0;
    }

    public function totalEarnedFromRefferals() {
        return DB::table('transactions')
                ->where('user_id', \Auth::id())
                ->where('type', \App\Transaction::TRANS_TYPE_REFFERAL)
                ->sum('ammount') ?? 0;
    }
}
